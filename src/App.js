import { robotSim } from './robot'
import './App.css';
import { useState } from 'react'
import botPic from './robot.png'

function App() {

  const [robot, setRobot] = useState(
    { position: '0,0',
      direction: 'north',
      lastTurn: ''
    })
  const [inputValue, setInputValue] = useState('')
  const [screen, setScreen] = useState('startScreen')
  const [moveList, setMoveList] = useState([])
  const [appDisplayed, setAppDisplay] = useState(true)
  const [goalID, setGoalID] = useState('goal-square')
  const [botAngle, rotateBot] = useState('')
  
  
  const array = Array.from(Array(9).keys())
  const coords = array.map((_,i) => array.map((_,k) => [i,k]))
  
  
  function randomCoordinate() {
    const x = Math.floor(Math.random() * (8 - 1) + 1)
    const y = Math.floor(Math.random() * (8 - 1) + 1)
    return [x,y]
  }
  
  const generateMines = () => {
    const array = Array.from(Array(7).keys())
    const newMines = []
    for (var i in array) {
      newMines.push(randomCoordinate())
    }
    return newMines
  }
  
  const [mines, setMines] = useState(generateMines())
  
  const mineExistsHere = (currentSquare) => {
    const mine = mines.find(x => x.toString() === currentSquare)
    return mine !== undefined
  } 
  
  const squares = 
  coords.flat().map( x => 
    <Square 
    key={x.toString()} 
    position={x.toString()} 
    robotIsHere={x.toString() == robot.position ? true : false } 
    mineIsHere={mineExistsHere(x.toString())}
    facing={botAngle}
    />
    )
    
  const storeSequence = () => {
    const newlist = moveList.concat({value: inputValue, id: 5})
    setMoveList(newlist)
  }

  const landedOnMine = (robotPosition) =>  {
    const isOnMine = mines.find(mine => mine.toString() === robotPosition)

    return isOnMine !== undefined
  }

  // const reachedGoal (robotPosition) => {

  // }

  // const gameOver = () => {

  // }

  const simButtonClicked = () => {
    const newRobot = robotSim(robot, inputValue)
    // newRobot.positionsPassed.map( square => console.log(square))
    setRobot(newRobot)
    storeSequence()
    setInputValue('')

    const directions = {
      north: 'bot-north',
      east: 'bot-east',
      south: 'bot-south',
      west: 'bot-west'
    }
    // rotateBot(directions[newRobot.direction])

    if(landedOnMine(newRobot.position)){
      setScreen('gameOverScreen')
    }
    
    if(newRobot.position.toString() === '8,8') {
      setScreen('winScreen')
    }
  }

  const sequenceButtonClicked = (event) => {
    const sequence = event.target.innerText
    setInputValue(sequence)
  }

  const startButtonClicked = () => {
    setScreen('playScreen')
  }

  const handleChange = (event) => {
    setInputValue(event.target.value)
  }

  return (
    <div class='container'>
      <Screen screenToDisplay={screen} screenChangeFunction={startButtonClicked}/>
    <div className='App'>
      <h1> Robot Factory </h1>
        <input value={inputValue} onChange={handleChange}/>
        <button onClick={simButtonClicked}> Run simulator!</button>
        <div className='board'>
          { squares }
        </div>
    </div>
        <Sequences list={moveList} changeInput={sequenceButtonClicked} />
    </div>
  );
}

function Square(props) {
  return (
    <div id={props.position == [8,8] ? 'goal-square' : 'noo'} className='square'>
      { props.robotIsHere ? <Robot facing={props.facing}/> : null }
      {props.mineIsHere ? <Mine /> : null}
    </div>
  )
}

function Robot(props) {
  return (
    <img src={botPic} alt="R" id="bot" class={props.facing}></img>
  )
}

function StartScreen(props) {
  return (
  <div class='start-screen'>
    <p id="welcome"> Welcome to the Robot Factory </p>
    <button onClick={props.buttonFunction} id="start-button"> Click here to play! </button>
  </div>
  )
}

function GameOverScreen(props) {
  return (
  <div class='game-over-screen'>
    <p id="welcome"> Oh no! You landed on a mine! Refresh for another attempt! </p>
  </div>
  )
}

function WinScreen(props) {
  return (
  <div class='win-screen'>
    <p id="welcome"> Congratulations, you've reached the end! </p>
  </div>
  )
}

function Screen(props) {
  const screens = {
    startScreen: <StartScreen buttonFunction={props.screenChangeFunction}/>,
    playScreen: "",
    gameOverScreen: <GameOverScreen />,
    winScreen: <WinScreen />
   }
  return(
    screens[props.screenToDisplay]
  )
}

function Sequences(props) {
  const list = props.list
  return(list.map(element => <li><button key={element.id} onClick={props.changeInput}> {element.value} </button></li>))
}

function Mine(props) {
  return <p class="mines"> M </p>    
}

export default App;
