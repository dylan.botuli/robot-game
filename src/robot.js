const bot = {
    position: '7,3',
    direction: 'north'
}

const getNewDirection = (currentDirection, nextMove) => {
    const resultingFacings = {
        north: {L: 'west', R: 'east'},
        east: {L: 'north', R: 'south'},
        west: {L: 'south', R: 'north'},
        south: {L: 'east', R: 'west'}
    }

    const possibleMoves = resultingFacings[currentDirection]
    return possibleMoves[nextMove]
}

const move = (currentPosition, directionFacing) => {
    const newPosition = currentPosition

    if (directionFacing == 'north') newPosition[1] += 1
    if (directionFacing == 'east') newPosition[0] += 1
    if (directionFacing == 'west') newPosition[0] -= 1
    if (directionFacing == 'south') newPosition[1] -= 1
    return newPosition
}

export const robotSim = (robot, sequence) => {
    const sequenceArray = Array.from(sequence)
    var newDirection = robot.direction
    var newPosition = robot.position.split(',').map(x => Number(x))
    var lastTurn = ''

    sequenceArray.forEach(character => {
        if (character == 'A') {

            newPosition = move(newPosition, newDirection)
        } 
        else {
            newDirection = getNewDirection(newDirection, character)
            lastTurn = character

        }
    })


    return {position: newPosition.toString(), direction: newDirection, lastTurn}
}

